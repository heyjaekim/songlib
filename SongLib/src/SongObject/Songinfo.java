package SongObject;

public class Songinfo {

	private String title;  // holds the name of song
	private String artist; // holds the artist's name
	private String album;  // holds the album's name
	private int year;  // holds the year album was created
	
	public Songinfo(String name, String artist) { // constructor
		
		this(name, artist, "", 0);
	}
	
	public Songinfo(String name, String artist,  String album) { // 2nd constructor
		
		this(name, artist, album, 0);
	}
	
	public Songinfo(String name, String artist, int year) { // 3rd constructor
		this(name, artist, "", year);
	}
	
	public Songinfo(String name, String artist, String album, int year) {
		
		this.title = name;
		this.artist = artist;
		this.album = album;
		this.year = year;
	}
	
	public String getTitle() {  
		
		return title;
	}
	
	public String getArtist() { 
		
		return artist;
	}
	
	public String getAlbum() {  
		
		return album;
	}
	
	public int getYear() {  
		
		return year;
	}
	
	public void setTitle(String songName) {  		
		title = songName;
	}
	
	public void setArtist(String artistName) {  
		
		artist = artistName;
	}
	
	public void setAlbum(String albumName) {
		
		album = albumName;
	}
	
	public void setYear(int songYear) {  
		
		this.year = songYear;
	}
	
	
	public String toString() {
		
		return title + " by " + artist;
	}

}