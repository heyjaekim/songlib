package SongObject;

import java.util.*;

public class SongCompare implements Comparator<Songinfo> {
	
	//overriding the song files
	public int compare(Songinfo s1, Songinfo s2) {
		if(s1.getTitle().toLowerCase().compareTo(s2.getTitle().toLowerCase()) == 0) {
			
			return s1.getArtist().toLowerCase().compareTo(s2.getArtist().toLowerCase());
		}
		return s1.getTitle().toLowerCase().compareTo(s2.getTitle().toLowerCase());
	}
	
}
